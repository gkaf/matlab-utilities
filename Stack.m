classdef Stack
    %Stack Class implementing a Stack data structure
    %   The class implement the stack data type using a singly linked list.
    
    % MatlabUtilities/Stack.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        head
        n_elem
    end
    
    methods
        function [stack] = Stack()
            stack.head = [];
            stack.n_elem = 0;
        end
        
        function [stack] = push(stack, x)
            botom = stack.head;
            top = SinglyLinkedList(x);
            stack.head = top.link(botom);
            stack.n_elem = stack.n_elem + 1;
        end
        
        function [stack, x] = pop(stack)
            if ~isempty(stack.head)
                x = stack.head.content();
                stack.head = stack.head.tail;
                stack.n_elem = stack.n_elem - 1;
            else
                x = [];
            end
        end
        
        function [empty] = isEmpty(stack)
            empty = (stack.n_elem == 0);
        end
        
        function [x] = top(stack)
            x = stack.head.content();
        end
        
        function [s] = size(stack)
            s = stack.n_elem;
        end
        
        function [elems] = toCell(stack)
            elems = cell(1, stack.n_elem);
            
            ls = stack;
            n = stack.n_elem;
            while not(ls.isEmpty())
                [ls, x] = ls.pop();
                elems{n} = x;
                n = n - 1;
            end
        end
    end
       
    methods (Static)
        function [ls] = fromVector(c)
            len = size(c,2);
            ls = Stack();
            for n = 1:len
                ls = ls.link(c(:,n));
            end
        end
        
        function [ls] = fromCell(c)
            len = length(c);
            ls = Stack();
            for n = 1:len
                ls = ls.link(c{n});
            end
        end
    end
    
end

