classdef Queue
    %Queue Class implementing the queue data struture
    %   The class implements the queue abstract data type using a double
    %   linked list.
    
    % MatlabUtilities/Queue.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        head
        tail
        n_elem
    end
    
    methods
        function [queue] = Queue()
            queue.head = [];
            queue.tail = [];
            queue.n_elem = 0;
        end
        
        function [queue] = preappend(queue, front_queue)
            if ~isempty(queue.tail)
                if ~isempty(front_queue.tail)
                    queue.head.linkFront(front_queue.tail);
                    front_queue.tail.linkBack(queue.head);
                    queue.head = front_queue.head;
                    
                    queue.n_elem = queue.n_elem + front_queue.n_elem;
                end
            else
                queue = front_queue;
            end
        end
        
        function [queue] = enqueue(queue, x)
            dll = DoublyLinkedList(x);
            
            dll.linkFront(queue.tail);
            if isempty(queue.tail)
                queue.head = dll;
            else
                queue.tail.linkBack(dll);
            end
            queue.tail = dll;
            
            queue.n_elem = queue.n_elem + 1;
        end
        
        function [queue, x] = dequeue(queue)
            if ~isempty(queue.head)
                x = queue.head.content();
                
                queue.head = queue.head.back;
                if ~isempty(queue.head)
                    queue.head.linkFront([]);
                else
                    queue.tail = [];
                end
                
                queue.n_elem = queue.n_elem - 1;
            else
                x = [];
            end
        end
        
        function [empty] = isEmpty(queue)
            empty = (queue.n_elem == 0);
        end
        
        function [s] = size(queue)
            s = queue.n_elem;
        end
        
        function [elems] = toCell(queue)
            elems = cell(1, queue.n_elem);
            
            ls = queue;
            n = 1;
            while not(ls.isEmpty())
                [ls, x] = ls.dequeue();
                elems{n} = x;
                n = n + 1;
            end
        end
    end
    
end

