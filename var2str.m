function [ str ] = var2str( ~ )
%var2str Returns the name of the argument if it is a variable.
%   If the argument is a variable it will return its name; otherwise it
%   returns the empty string.

% MatlabUtilities/var2str.m
% Author: Georgios Kafanas
% Institute: University of Bristol
% Year: 2018
% Contact: georgios.kafanas@bristol.ac.uk

str = inputname(1);

end

