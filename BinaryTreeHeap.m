classdef (Abstract) BinaryTreeHeap %< handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [bh] = BinaryTreeHeap()
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
        end
    end
    
    methods (Abstract)
        [s] = toString(bh);
        [bh] = addElement(bh, keyedElement);
        [n] = minDepth(bh);
        [n] = maxDepth(bh);
        [flag] = isEmpty(bh);
    end
end

