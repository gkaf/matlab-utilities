classdef SinglyLinkedList
    %SinglyLinkedList Class implementing a single linked list
    %   Typical implementation of a single linked list, with one element
    %   and a pointer either to the next list element or null.
    
    % MatlabUtilities/SinglyLinkedList.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        elem
        tail
    end
    
    methods
        function [sll] = SinglyLinkedList(x)
            sll.elem = x;
            sll.tail = [];
        end
        
        function [sll] = link(sll, sslTail)
            sll.tail = sslTail;
        end
        
        function [x] = content(sll)
            x = sll.elem;
        end
    end
    
end

