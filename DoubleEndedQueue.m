classdef DoubleEndedQueue
    %DoubleEndedQueue Clas implementing the double ended queue abstract
    %data type.
    %   The class implement the double ended queue abstract data type using
    %   a double linked list.
    
    % MatlabUtilities/DoubleEndedQueue.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        head
        tail
        n_elem
    end
    
    methods
        function [deq] = DoubleEndedQueue()
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            deq.head = [];
            deq.tail = [];
            deq.n_elem = 0;
        end
        
        function [iter] = getIterator(deq)
            iter = DoubleEndedQueueIterator(deq);
        end
        
        function [x] = front(deq)
            dll = deq.head;
            if ~isempty(dll)
                x = dll.content();
            else
                x = [];
            end
        end
        
        function [x] = back(deq)
            dll = deq.tail;
            if ~isempty(dll)
                x = dll.content();
            else
                x = [];
            end
        end
        
        function [deq] = addBack(deq, x)
            dll = DoublyLinkedList(x);
            
            dll.linkFront(deq.tail);
            if isempty(deq.tail)
                deq.head = dll;
            else
                deq.tail.linkBack(dll);
            end
            deq.tail = dll;
            
            deq.n_elem = deq.n_elem + 1;
        end
        
        function [deq] = addFront(deq, x)
            dll = DoublyLinkedList(x);
            
            dll.linkBack(deq.head);
            if isempty(deq.head)
                deq.tail = dll;
            else
                deq.head.linkFront(dll);
            end
            deq.head = dll;
            
            deq.n_elem = deq.n_elem + 1;
        end
        
        function [deq, x] = removeFront(deq)
            if ~isempty(deq.head)
                x = deq.head.content();
                
                deq.head = deq.head.back;
                if ~isempty(deq.head)
                    deq.head.linkFront([]);
                else
                    deq.tail = [];
                end
                
                deq.n_elem = deq.n_elem - 1;
            else
                x = [];
            end
        end
        
        function [deq, x] = removeBack(deq)
            if ~isempty(deq.tail)
                x = deq.tail.content();
                
                deq.tail = deq.tail.front;
                if ~isempty(deq.tail)
                    deq.tail.linkBack([]);
                else
                    deq.head = [];
                end
                
                deq.n_elem = deq.n_elem - 1;
            else
                x = [];
            end
        end
        
        function [deq] = preappend(deq, front_deq)
            if ~isempty(deq.tail)
                if ~isempty(front_deq.tail)
                    deq.head.linkFront(front_deq.tail);
                    front_deq.tail.linkBack(deq.head);
                    deq.head = front_deq.head;
                    
                    deq.n_elem = deq.n_elem + front_deq.n_elem;
                end
            else
                deq = front_deq;
            end
        end
        
        function [deq_1] = flip(deq_0)
            nr = DoubleEndedQueue();
            
            r = deq_0;
            while ~r.isEmpty()               
                [r, x] = r.removeBack();
                nr = nr.addBack(x);
            end
            
            deq_1 = nr;
        end
        
        function [ind] = isEmpty(deq)
            ind = (deq.n_elem == 0);
        end
        
        function [s] = size(deq)
            s = deq.n_elem;
        end
        
        function [deq_1] = copy(deq_0)
            nr = DoubleEndedQueue();
            
            ls = deq_0.head;
            while ~isempty(ls)
                x = ls.content();
                nr = nr.addBack(x);
                ls = ls.back;
            end
            
            deq_1 = nr;
        end
        
        function [elems] = toCell(deq)
            elems = cell(1, deq.n_elem);
            
            ls = deq.head;
            n = 1;
            while ~isempty(ls)
                x = ls.content();
                elems{n} = x;
                n = n + 1;
                ls = ls.back;
            end
        end
    end
end

