classdef BinaryHeap
    %BinaryHeap A binary tree implementation of a heap.
    %   A persistent heap implemented with a binary tree.
    %       Size complexity: O(lg(n))
    %       Time complexity:
    %           - addElement: O(lg(n))
    %           - extractElement: O(lg(n))
    %           - size: O(1)
    %           - isEmpty: O(1)
    %           - toString: O(n)
    
    properties
        binaryHeapTree
        n_elements
    end
    
    methods
        function [bh] = BinaryHeap()
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            bh.binaryHeapTree = BinaryTreeHeapLeaf();
            bh.n_elements = 0;
        end
        
        function [bh] = addElement(bh, keyedElement)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            bh.binaryHeapTree = bh.binaryHeapTree.addElement(keyedElement);
            bh.n_elements = bh.n_elements + 1;
        end
        
        function [bh, keyedElement] = extractElement(bh)
            if ~bh.binaryHeapTree.isEmpty()
                [bh.binaryHeapTree, keyedElement] = bh.binaryHeapTree.extractElement();
                bh.n_elements = bh.n_elements - 1;
            else
                keyedElement = {};
            end
        end
        
        function [flag] = isEmpty(bh)
            flag = bh.binaryHeapTree.isEmpty();
        end
        
        function [n] = size(bh)
            n = bh.n_elements;
        end
        
        function [s] = toString(bh)
            s = bh.binaryHeapTree.toString();
        end
    end
end

