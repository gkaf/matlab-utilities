classdef BinaryTreeHeapLeaf < BinaryTreeHeap
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [bh] = BinaryTreeHeapLeaf()
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            bh@BinaryTreeHeap();
        end
        
        function [n] = minDepth(~)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            n = 0;
        end
        
        function [n] = maxDepth(~)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            n = 0;
        end
        
        function [s] = toString(~)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            s = 'e';
        end
        
        function [flag] = isEmpty(~)
            flag = true;
        end
        
        function [bh] = addElement(~, keyedElement)
            bh = BinaryTreeHeapNode(keyedElement);
        end
    end
end

