classdef BinaryTreeHeapNode < BinaryTreeHeap
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        left
        right
        
        keyedElement
        
        min_depth
        max_depth
    end
    
    methods
        function [bh] = BinaryTreeHeapNode(keyedElement)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            bh@BinaryTreeHeap();
            
            bh.left = BinaryTreeHeapLeaf();
            bh.right = BinaryTreeHeapLeaf();
            
            bh.keyedElement = keyedElement;
            
            bh.min_depth = 1;
            bh.max_depth = 1;
        end
        
        function [n] = minDepth(bh)
            n = bh.min_depth;
        end
        
        function [n] = maxDepth(bh)
            n = bh.max_depth;
        end
        
        function [s] = toString(bh)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            s_left = bh.left.toString();
            s_right = bh.right.toString();
            s_elem = bh.keyedElement.toString();
            s = ['[ ', s_left, ', ', s_elem, ', ', s_right, ' ]'];
        end
        
        function [bh, keyedElement] = extractElement(bh)
            keyedElement = bh.keyedElement;
            [bh, new_keyedElement] = bh.removePrevious();
            if ~bh.isEmpty()
                bh.keyedElement = new_keyedElement;
                bh = bh.heapify();
            end
        end
        
        function [bh, keyedElement] = removePrevious(bh)
            current = bh;
            if current.left.isEmpty() && current.right.isEmpty()
                keyedElement = current.keyedElement;
                bh = BinaryTreeHeapLeaf();
            else
                if current.left.maxDepth() > current.right.maxDepth()
                    [current.left, keyedElement] = current.left.removePrevious();
                else
                    [current.right, keyedElement] = current.right.removePrevious();
                end
                
                current.min_depth = min(current.right.minDepth(), current.left.minDepth()) + 1;
                current.max_depth = max(current.right.maxDepth(), current.left.maxDepth()) + 1;
                
                bh = current;
            end
        end
        
        function [bh] = heapify(bh)
            if ~bh.left.isEmpty()
                if bh.right.isEmpty()
                    parent_keyedElement = bh.keyedElement;
                    child_keyedElement = bh.left.keyedElement;
                    if child_keyedElement.key() < parent_keyedElement.key()
                        bh.keyedElement = child_keyedElement;
                        bh.left.keyedElement = parent_keyedElement;
                        bh.left = bh.left.heapify();
                    end
                else
                    if bh.right.keyedElement.key() < bh.left.keyedElement.key()
                        parent_keyedElement = bh.keyedElement;
                        child_keyedElement = bh.right.keyedElement;
                        if child_keyedElement.key() < parent_keyedElement.key()
                            bh.keyedElement = child_keyedElement;
                            bh.right.keyedElement = parent_keyedElement;
                            bh.right = bh.right.heapify();
                        end
                    else
                        parent_keyedElement = bh.keyedElement;
                        child_keyedElement = bh.left.keyedElement;
                        if child_keyedElement.key() < parent_keyedElement.key()
                            bh.keyedElement = child_keyedElement;
                            bh.left.keyedElement = parent_keyedElement;
                            bh.left = bh.left.heapify();
                        end
                    end
                end
            end
        end
        
        function [bh] = addElement(bh, keyedElement)
            current = bh;
            if keyedElement.key() < current.keyedElement.key()
                new_keyedElement = current.keyedElement;
                current.keyedElement = keyedElement;
                keyedElement = new_keyedElement;
            end
            
            if current.left.minDepth() > current.right.minDepth()
                current.right = current.right.addElement(keyedElement);
            else
                current.left = current.left.addElement(keyedElement);
            end
            
            current.min_depth = min(current.right.minDepth(), current.left.minDepth()) + 1;
            current.max_depth = max(current.right.maxDepth(), current.left.maxDepth()) + 1;
            
            bh = current;
        end
        
        function [flag] = isEmpty(~)
            flag = false;
        end
    end
end

