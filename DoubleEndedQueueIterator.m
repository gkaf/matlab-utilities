classdef DoubleEndedQueueIterator
    %DoubleEndedQueueIterator An iterator over the elements of a double
    %ended queue.
    %   The class implements an iterator over a double eneded queue. This
    %   is ncessary, as the double eneded queue is implementes using a
    %   doubly linked list which is a mutable data structure, and simplly
    %   itterating over a double eded queu will make elements unreachable.

    % MatlabUtilities/DoubleEndedQueueIterator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        head_doublyLinkedList
        tail_doublyLinkedList
    end

    methods
        function [iter] = DoubleEndedQueueIterator(doubleEndedQueue)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            iter.head_doublyLinkedList = doubleEndedQueue.head;
            iter.tail_doublyLinkedList = doubleEndedQueue.tail;
        end

        function [ind] = hasTail(iter)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            ind = ~isempty(iter.tail_doublyLinkedList);
        end

        function [ind] = hasHead(iter)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            ind = ~isempty(iter.head_doublyLinkedList);
        end

        function [iter] = nextTail(iter)
            iter.tail_doublyLinkedList = iter.tail_doublyLinkedList.front;
        end

        function [iter] = nextHead(iter)
            iter.head_doublyLinkedList = iter.head_doublyLinkedList.back;
        end

        function [x] = getTail(iter)
            x = iter.tail_doublyLinkedList.elem;
        end

        function [x] = getHead(iter)
            x = iter.head_doublyLinkedList.elem;
        end
    end
end
