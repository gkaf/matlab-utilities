classdef DoublyLinkedList < handle
    %DoublyLinkedList Class implementing a double linked list
    %   Typical implementation of a double linked list, with one one object
    %   per list element and two pointers, one pointing to the next and one
    %   to the previous element of the list.
    
    % MatlabUtilities/DoublyLinkedList.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        elem
        front
        back
    end

    methods
        function [dll] = DoublyLinkedList(x)
            dll.elem = x;
            dll.front = [];
            dll.back = [];
        end

        function linkFront(dll, sslFront)
            dll.front = sslFront;
        end

        function linkBack(dll, sslBack)
            dll.back = sslBack;
        end

        function [x] = content(dll)
            x = dll.elem;
        end
    end

end
